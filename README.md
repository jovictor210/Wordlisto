<h1 style="color:red">#Wordlisto</h1>
<h2>Custom list generator, incremented by subject. </h2>
<p>
The purpose of this program is create a custom wordlist to use in pentests and studies. </br>
The tool receive some informations about the password owner, and with it can be generated at least 10.000 authentication possibilities. </br>
</p>
<p>
Program written in C language</br>
Install with "sudo sh install.sh"</br>
Portable for Linux and Windows</br>
</p>
<p>
<h2> #The usage</h2> </br>
The arguments refer to the type of matter that should be combined with user input. </br>
./wordlisto [-s -c -m -t -w -a -l || --custom <path> --help] </br>
-s simple
-c common
-m music
-t tech
-w show
-a anime
-l all previous
</br>
--custom: use a custom wordlist (should be used --custom "path")</br>
--help: usage screen.
</br>
</br>
</p>
acess the man with "man wordlisto"
